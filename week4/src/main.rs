use actix_files::Files;
use actix_web::{get, web, App, HttpResponse, HttpServer, Responder};
use rand::seq::SliceRandom;
use rand::Rng; // Import for selecting a random element from a slice

// Function to return a random quote
async fn quote_of_the_day() -> impl Responder {
    let quotes = [
        "The only way to do great work is to love what you do. -Steve Jobs",
        "Life is what happens when you're busy making other plans. -John Lennon",
        "Get busy living or get busy dying. -Stephen King",
    ];

    // Randomly select a quote from the array
    let quote = quotes
        .choose(&mut rand::thread_rng())
        .unwrap_or(&"No quote found");

    HttpResponse::Ok().body(*quote)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            // Make sure your route goes first before serving other files
            .route("/quote_of_the_day", web::get().to(quote_of_the_day)) // Route to the quote_of_the_day function
            .service(Files::new("/", "./static/root/").index_file("index.html"))
    })
    .bind(("0.0.0.0", 50505))?
    .run()
    .await
}

#[cfg(test)]
mod tests {
    use actix_web::{body::to_bytes, dev::Service, http, test, web, App, Error};

    use super::*;

    #[actix_web::test]
    async fn test_quote_of_the_day() -> Result<(), Error> {
        let app = App::new().route("/quote_of_the_day", web::get().to(quote_of_the_day));
        let app = test::init_service(app).await;

        let req = test::TestRequest::get()
            .uri("/quote_of_the_day")
            .to_request();
        let resp = app.call(req).await?;

        assert_eq!(resp.status(), http::StatusCode::OK);
        Ok(())
    }
}
